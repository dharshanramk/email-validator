Check if the email address is valid and print only valid email addresses in lexicographical order.

Valid email addresses must follow these rules:

● It must have the username@websitename.extension format type.
● The username can only contain letters, digits, dashes and underscores.[ a-z], [A-Z], [0-9], -, _
● The website name can only have letters and digits .
● The extension can only contain letters and the maximum length of the extension is 3

Sample Input:

xa_bc@gmail.com
cdf%@yahoo.com
dab123@gmail.com

Output:

dab123@gmail.com
xa_bc@gmail.com
